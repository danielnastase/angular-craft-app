var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var clean  = require('gulp-clean');
var templateCache = require('gulp-angular-templatecache');
var uglifycss = require('gulp-uglifycss');
var runSequence = require('run-sequence');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
var babel = require('gulp-babel');



gulp.task('uglify-css', function () {
  gulp.src('./build/**/*.css')
    .pipe(uglifycss())
   	.pipe(gulp.dest('./dist/'));
});

gulp.task('compile-less', function() {
    return gulp.src('./styles/style.less')  // only compile the entry file
        .pipe(less())
        .pipe(gulp.dest('./build/'))
});

gulp.task('prepare-css', function(done) {
    runSequence('compile-less', 'uglify-css', function() {
        console.log('CSS Prepare done');
        done();
    });
});

gulp.task('clean', function () {
  return gulp.src(['./dist', './build'], {read: false})
    .pipe(clean());
});

gulp.task('min-js', function () {
  return gulp.src('./build/app.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist'));
});

gulp.task('babel-compile', () => {
    return gulp.src('./build/es6-app.js')
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(rename("app.js"))
    .pipe(gulp.dest('build'));
});

gulp.task('concat-js', function() {
  return gulp.src('./src/**/*.js')
    .pipe(concat('es6-app.js'))
    .pipe(gulp.dest('./build/'));
});

gulp.task('prepare-js', function(done) {
    runSequence('concat-js', 'babel-compile', 'min-js', function() {
        console.log('JS Prepare done');
        done();
    });
});

gulp.task('templates-to-js', function() {
  return gulp.src('./src/**/*.html')
    .pipe(templateCache({standalone: true}))
    .pipe(gulp.dest('dist'));
});


gulp.task('watch', function() {
    gulp.watch(['./styles/**/*', './src/**/*'], ['build']);  // Watch all the .less files, then run the less task
});

gulp.task('make-build', ['prepare-js', 'prepare-css', 'templates-to-js'])

gulp.task('build', function(done) {
    runSequence('clean', 'make-build', function() {
        console.log('Make Build bone');
        done();
    });
});

gulp.task('default', ['build', 'watch']); // Default will run the 'entry' watch taskgul