myApp.service('appService', function($http) {


	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	let BASE_URL = "./php/service.php";

	function doCall(url, oper, params) {
		let requestParams = params || {};
		requestParams.oper = oper;
		return $http.post(
		 	url, 
		 	requestParams
		);
	}

	this.getItems = function(params) {
		return doCall(BASE_URL, "getItems", params);
	}

	this.vote = function(params) {
		return doCall(BASE_URL, "vote", params);
	}

	this.registerUser = function(profile) {
		return doCall(BASE_URL, "registerUser", profile);
	}

	this.addItem = function(item) {
		return doCall(BASE_URL, "userPublishItem", item);
	}

	this.subscribeToNewsletter = function(email) {
		return doCall(BASE_URL, "subscribeToNewsletter", {email:email});
	}

	return this;
});