let myApp = angular.module("myApp", ['ui.router', 'templates']);

myApp.controller("appCtrl", function($scope, authService, appService){
  $scope.authService = authService;
});

myApp.run(function($rootScope, $state, authService, $location, utilsService){

  let self = this;

  function blockIfNeedAuth(event, toState, toParams, fromState, fromParams) {
    if (toState !== undefined) {
      if ('data' in toState) {
        if(toState.data.needsLoggedin && !authService.isLoggedin()) {
          authService.saveRedirectToLocation(toState.name, toParams);
          event.preventDefault();
          $state.go('login');
        }
      }
    }
  }

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if (!authService.isLoggedin() && (!toState.callInProgress)) {
      let idToken = localStorage.getItem("ac-id-token");
      if ((idToken != null) && (idToken.length > 0)) {
        event.preventDefault();
        toState.callInProgress = true;
        authService.getAuth0().getProfile(
          idToken,
          function (err, profile) {
            toState.callInProgress = false;
            if (err) {
              alert("User could not be authenticated !");
              console.log(err);
              localStorage.removeItem("ac-id-token");
            }
            else {
              profile = authService.normalizeUserProfile(profile);
              authService.setIdToken(idToken);
              authService.setUserProfile(profile);
            }
            if(Object.keys($location.search()).length > 0) {
              utilsService.setBookmarkletData($location.search());
            }
            $state.go(toState.name, toParams);
          }
        );
      }
      else {
        blockIfNeedAuth(event, toState, toParams, fromState, fromParams);
      }
    }
  });
});


myApp.config(function($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider) {

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise(function(){
      let n = location.href.lastIndexOf('/');
      let urlParamsString = location.href.substring(n + 1);
      if ((urlParamsString.indexOf("access_token") != -1) && (urlParamsString.indexOf("access_token") != -1)) {
        return '/login?' + urlParamsString;
      }
      else {
        return "/feed/rating/"
      }
    });

    $stateProvider
      .state('feed', {
            url: '/feed/:orderBy/:page',
            templateUrl: 'components/feed/feed.html',
            controller: "feedCtrl"
      })
      .state('login', {
            url: '/login',
            templateUrl: 'components/login/login.html',
            controller: "loginCtrl"
      })
      .state('add', {
            url: '/add',
            templateUrl: 'components/add/add.html',
            controller: "addCtrl",
            data: {
              needsLoggedin: true
            }
      })
      .state('blog', {
            url: '/blog',
            templateUrl: 'components/comingsoon/comingsoon.html'
      })
      .state('about', {
            url: '/about',
            templateUrl: 'components/comingsoon/comingsoon.html'
      })
      .state('contact', {
            url: '/contact',
            templateUrl: 'components/comingsoon/comingsoon.html'
      })
      .state('logout', {
            url: '/logout',
            templateUrl: 'components/logout/logout.html',
            controller: "logoutCtrl"
      });
  }
);