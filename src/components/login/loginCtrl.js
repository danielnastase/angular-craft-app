myApp.controller("loginCtrl", function($scope, authService, $location, $state, utilsService, appService) {

	

	let urlPrams = utilsService.getUrlPrams();


	let access_token = urlPrams.access_token;
  	let id_token = urlPrams.id_token;

  	if (angular.isDefined(access_token)) {
  		authService.setAccesToken(access_token);
  	}

	if (angular.isDefined(id_token)) {
		authService.getAuth0().getProfile(
		  id_token,
		  function (err, profile) {
		    localStorage.setItem("ac-id-token", id_token);
		    profile = authService.normalizeUserProfile(profile);
		    authService.setIdToken(id_token);
		    authService.setUserProfile(profile);
		    appService.registerUser(authService.getUserProfile());
		    redirectAfterLogin();
		  }
    	);
	};

	function redirectAfterLogin() {
		if(authService.mustRedirectToLocation()) {
			let redirectTo = authService.retriveAndClearRedirectToLocation();
			$state.go(redirectTo.stateName, redirectTo.params);
		}
		else {
			$state.go('feed', {orderBy: "rating"});
		}
	}


	$scope.loginWithTwitter = function() {
    	authService.login('twitter');
	};

	$scope.loginWithGoogle = function() {
    	authService.login('google-oauth2');
	};



});