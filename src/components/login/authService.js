myApp.service('authService', function() {

	this.user = null;
	this.idToken = null;
	this.accesToken = null;

	let self = this;

	let auth0 = new Auth0({
	    domain:       'jscraft.eu.auth0.com',
	    clientID:     'KGImifzeytV0dvypt9BNPBQIOSHK6EjN'
	});

	this.isLoggedin = function() {
		return (this.idToken != null) && (this.idToken.length > 0) ;
	}

	this.setAccesToken = function(accesToken) {
		this.accesToken = accesToken;
	}

	this.getAccesToken = function() {
		return this.accesToken;
	}

	this.setUserProfile = function(profile) {
		this.user = profile;
		this.user.isAdmin = (profile.userId == "google-oauth2|108184769256174077804");
	}

	this.normalizeUserProfile = function(auth0Profile) {
		let type = auth0Profile.user_id.indexOf("twitter") != -1 ? "twitter" : "gmail"; 
		let normalizedProfile = {
			userId: auth0Profile.user_id,
			type: type,
			name: auth0Profile.name,
			picture: auth0Profile.picture,
			contact: type == "twitter" ?  auth0Profile.screen_name : auth0Profile.email
		};
		return normalizedProfile;
	}

	this.getUserProfile = function() {
		return self.user;
	}


	this.setIdToken = function(idToken) {
		self.idToken = idToken;
	}

	this.login = function(type) {
		auth0.login({
      		connection: type,
      		callbackOnLocationHash: true
    	});
		
	}

	this.getAuth0 = function() {
		return auth0;
	}

	this.logout = function() {
		self.idToken = null;
		self.user = null;
		self.accesToken = null;
	}

	this.saveRedirectToLocation = function(toStateName, toStateParams) {
		localStorage.setItem("ac-auth-redirect-to-state" , toStateName);
		if (toStateParams) {
			let stateParamsString = JSON.stringify(toStateParams);
			localStorage.setItem("ac-auth-redirect-to-state-params" , stateParamsString);
		}
	}

	this.mustRedirectToLocation = function() {
		let stateToRedirect = localStorage.getItem("ac-auth-redirect-to-state");
   		return ((angular.isDefined(stateToRedirect)) && (stateToRedirect != null));
	}
	

	this.retriveAndClearRedirectToLocation = function() {
		let redirectLocation = {};
		
		redirectLocation.stateName = localStorage.getItem("ac-auth-redirect-to-state");
		localStorage.removeItem("ac-auth-redirect-to-state");

		let stateParamsString = localStorage.getItem("ac-auth-redirect-to-state-params");
		if (stateParamsString) {
			redirectLocation.params = JSON.parse(stateParamsString);
			localStorage.removeItem("ac-auth-redirect-to-state-params");
		}

		return redirectLocation;
	}



	return this;
});