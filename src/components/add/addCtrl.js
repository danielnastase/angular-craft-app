myApp.controller("addCtrl", function($scope, appService, authService, $state, utilsService) {

	$scope.isAdmin = authService.getUserProfile().isAdmin;

	function resetData() {
		$scope.newItem = {};
		$scope.itemAlreadyExistingError = false;
	}
	resetData();

	if(utilsService.getBookmarkletData()) {
		$scope.newItem.title =  utilsService.getBookmarkletData().addtitle;
		$scope.newItem.url = utilsService.getBookmarkletData().addlocation;
	}


	$scope.bots = [
		{id:'boot|1', name: 'Daniel N.'},
		{id:'boot|2', name: 'JS Craft'},
		{id:'boot|3', name: 'Ted Malone'}
	];

	$scope.oreDelay = [
		{value:0, label: '0 ore'},
		{value:1, label: '1 ora'},
		{value:3, label: '3 ore'},
		{value:6, label: '6 ore'}
	];

	$scope.zileDelay = [
		{value:0, label: '0 zile'},
		{value:1, label: '1 zi'},
		{value:2, label: '2 zile'},
		{value:3, label: '3 zile'},
		{value:4, label: '4 zile'},
		{value:5, label: '5 zile'},
		{value:6, label: '6 zile'},
		{value:7, label: '7 zile'}
	];




	$scope.addItem = function() {
		$scope.newItemForm.inputTitle.$touched = true;
		$scope.newItemForm.inputUrl.$touched = true;
		if ($scope.newItemForm.$valid) {
			$scope.newItem.userId = authService.getUserProfile().userId;
			if ($scope.isAdmin) {

				$scope.newItem.nrVoturi = $scope.newItem.nrVoturi || 0;
				$scope.newItem.zileDelay = $scope.newItem.zileDelay || 0;
				$scope.newItem.oreDelay = $scope.newItem.oreDelay || 0;

				if ($scope.newItem.botId != null) {
					$scope.newItem.userId = $scope.newItem.botId
				}
				else {
					$scope.newItem.userId = $scope.bots[Math.floor(Math.random()*$scope.bots.length)].id;
				}
			}
			appService.addItem($scope.newItem).then(
				(data) => {
					if (data.data.result == "done") {
						resetData();
						$state.go('feed', { 'orderBy':'date'});
					}
					if (data.data.result == "already existing") {
						$scope.itemAlreadyExistingError = true;
					}
				},
				(err) => {console.log(err);}
			)
		};
	};
});