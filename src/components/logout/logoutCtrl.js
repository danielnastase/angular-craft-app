myApp.controller("logoutCtrl", function($scope, authService, $state) {
	authService.logout();
    localStorage.removeItem("ac-id-token");
	$state.go('feed', {orderBy: "rating"});
});