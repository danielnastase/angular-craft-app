myApp.component('newsletterWidget', {
    templateUrl: 'components/newsletter-widget/newsletter-widget.html',

    controller: function ($scope, appService) {
        $scope.newsletterSubscribe = function() {
            if (!$scope.newsletterSubscribeForm.$invalid) {
              appService.subscribeToNewsletter($scope.subscriberEmail).then(
                (data) => {
                  $scope.subscriberEmail = "";
                  alert("You have beed subscribed to the JS-Craft newsletter !")
                },
                (err) => { alert("There has been an error !") }
              )
            }
            else {
              alert("Please insert a valid email address !")
            }
        }
    }
});