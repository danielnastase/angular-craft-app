myApp.controller("feedCtrl", function($scope, $location, $state, appService, authService, $stateParams) {

	$scope.params = {
		feedOrderedBy: $stateParams.orderBy ||  'rating',
		page: parseInt($stateParams.page) ||  0,
		userId: authService.isLoggedin() ? authService.getUserProfile().userId : -1 
	}

	$scope.authService = authService;



	function fetchItemsFromServer() {
		$scope.busy = true;
		appService.getItems($scope.params).then(
			(data) => {
				$scope.data = data.data;
				$scope.busy = false;
			},
			(err) => {
				$scope.data = err;
				alert("HTTP ERR");
			}
		)
	}
	
	fetchItemsFromServer();

	$scope.vote = function(item) {
		if(!authService.isLoggedin()) {
			authService.saveRedirectToLocation($state.current.name, $state.params);
			$state.go("login");
		}
		else {
			appService.vote(
				{
					itemId: item.id, 
					userId : authService.getUserProfile().userId
				}
			).then(
				(data) => {
					item.user_has_voted = !item.user_has_voted;
					item.user_has_voted ? item.votes++ : item.votes--;
				},
    			(err) => console.log(err)
			)
		}
	};
});